const logIn = require('./logIn');

(async () => {
  const fs = require('fs')
  
  let resultsObj = {
    pagesScraped: 0,
    emailsChecked: 0,
    contactArray: [],
  }

  let results = []
  let fallback = 0;
  const {page, browser, email} = await logIn();
  await page.waitFor(3500)
  let condition = true;
  let iterator = 0;
  let emailsChecked = 0;
  if (fs.existsSync(`results/${email}.json`)) {
    // Do something
    let data = JSON.parse(fs.readFileSync(`results/${email}.json`, 'utf8'));
    iterator = data.pagesScraped;
    emailsChecked = data.emailsChecked
    results = data.contactArray;
    console.log('What we have so far:', `${iterator} pages checked,
    ${emailsChecked} emails Checked,
    ${results.length} unique emails`)
}
  while(condition){
    console.time('page')

    await page.goto(`https://mail.google.com/mail/u/0/#all/in%3Ainbox/p${iterator}`)
    await page.waitFor(750)
    
      let rows = await page.$$('td.yX');
      let end = await page.$('tr.TD')
    let rowsOnPage = rows.length;
    let rowsOnLastPage = 0;
    console.log('rows: ', rowsOnPage)
    let holdRowNumber = 0;
  for(let i = 0; i<rowsOnPage; i++){
    // if(fallback>15){
    //   continue;
    // }
    
    try {
      let iterationRowCheck = await page.$$('td.yX')
      console.log('.av on this page', iterationRowCheck.length)
      holdRowNumber = iterationRowCheck.length;
      // if(iterationRowCheck.length > 101){
      //   let difference = iterationRowCheck.length - 100;
      //   iterationRowCheck = iterationRowCheck.splice(difference)
      //   console.log('difference', difference)
      //   console.log('spliced down to.', iterationRowCheck.length)
      // }
      if(!iterationRowCheck[i]){
        continue
      }
      if(!iterationRowCheck[i].boxModel()){
        console.log(i, "is not visible")
        continue;
      }
      // if(!isVisible(page, iterationRowCheck[i])){
      //   console.log('not visible')
      //   continue;
      // }
      try{
        await iterationRowCheck[i].click();
      }catch{
        continue;
      }
        await page.waitForNavigation();
      let closeDraftButton = await page.$('.Ha')
      console.log(closeDraftButton)
      if(closeDraftButton){
       await closeDraftButton.click()
      }

      console.log(i)
      let toEmails = await page.$$eval("span.hb > span[email]", elements => elements.map(el=>{return{name: el.getAttribute('name'),email: el.getAttribute('email')}}));
      let fromEmails = await page.$$eval("span.qu > span[email]", elements =>elements.map(el=>{return{name: el.getAttribute('name'),email: el.getAttribute('email')}}));
      console.log(results.length)
      emailsChecked++;
      console.log(toEmails, fromEmails)
      results = [...results, ...toEmails, ...fromEmails]
      await page.goto(`https://mail.google.com/mail/u/0/#all/in%3Ainbox/p${iterator}`)
      await page.waitFor(750)
    } catch (error) {
      console.log(error)
      console.log('couldnt click ', i)
      fallback+=1;
      await page.goto(`https://mail.google.com/mail/u/0/#all/in%3Ainbox/p${iterator}`)
      await page.waitFor(1000)
    }}
    results = results.filter((thing, index, self) =>
  index === self.findIndex((t) => (
    t.place === thing.place && t.name === thing.name
  ))
)
    uniq = [...new Set(results)];
      console.log(uniq, uniq.length)

      resultsObj.emailsChecked = emailsChecked;
      resultsObj.pagesScraped = iterator;
      resultsObj.contactArray = uniq;
      console.log(resultsObj)
      
      
      let json = JSON.stringify(resultsObj)
      await fs.writeFileSync(`results/${email}.json`, json, 'utf8')
    rowsOnLastPage = holdRowNumber;
    console.timeEnd('page')
    iterator++;
        if(end){
      console.log('we made it')
      condition = false;
    }
  }

 
  console.log('Finished on page '+iterator)
  console.log('If that is not your last page of emails, google likely paused your account access for too many page visits, try again in a little while.')
  await browser.close()
})();