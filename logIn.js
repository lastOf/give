const startUp = require('./startUp');


module.exports = async ()=>{
    let args = process.argv;
    let email = args.filter(el=>el.includes(':'))[0].replace(':', '')
    let password = args.filter(el=>el.includes('*'))[0].replace('*', '')
    const {page, browser} = await startUp(false);
    await page.goto('https://gmail.com');
    await page.waitFor(1000)
    let emailLogin = await page.$('#identifierId')

    await emailLogin.click('#identifierId')
    await page.keyboard.type(email)
    await page.keyboard.press('Enter')
    await page.waitForSelector('#password input')
    await page.waitFor(750)
    await page.keyboard.type(password)
    await page.keyboard.press('Enter')
    await page.waitFor(1500)
    return {page, browser, email};
}