const puppeteer = require('puppeteer-extra')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')

module.exports = async (hide)=>{
    puppeteer.use(StealthPlugin())
    const browser = await puppeteer.launch({headless:hide});

    const page = await browser.newPage();
        page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:66.0) Gecko/20100101 Firefox/66.0');
//     await page.setViewport({
//   width: 1840,
//   height: 880,
// //   deviceScaleFactor: 1,
// });
        // await page.setRequestInterception(true);
    
        // page.on('request', (req) => {
        //     if( req.resourceType() == 'font' || req.resourceType() == 'stylesheet' || req.resourceType() == 'image'){ //req.resourceType() == 'media' ||
        //         req.abort();
        //     }
        //     else if(req.url().includes('cloudflare') ||req.url().includes('youtube') ||req.url().includes('video') ||req.resourceType() == 'font' || req.resourceType() == 'image' ||req.resourceType() == 'stylesheet' || req.resourceType() == 'media'){
        //         // if(req.url().includes('video')){
        //         //     console.log('------------------->Knocked out a video<---------------------------------\n-----\n-----\n---------')
        //         // }
        //         // if(req.url().includes('cloudflare')){
        //         //     console.log('~~~~~~~~~~~~~~~~~~~>Removed cloudflare<~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n-----\n-----\n---------')
        //         // }
        //         req.abort();
        //     }
        //     else {
        //         req.continue();
        //     }
        // });
        return {page, browser};
}