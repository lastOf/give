# GiveMeBackMyEmail or GMBME

### What does it do?

GMBME runs through your gmail account and creates a list of all unique email addresses from your inbox.

### Why?

One of the things that has stopped me from leaving gmail again and again is leaving behind all those contacts. I could not find a simple way to export them.

### Current Reality

GMBME is currently at the proof of concept stage! 

Features:

* Searches emails on each page for all contacts
* Reduces to a list of unique email addresses
* Saves the last page checked so you can pick up where you left off
* Supports multiple scanning seperate accounts
* Pulls addressee's name data as well


Roadmap:

* Save to CSV
* Pull out entire emails
* handle more stuff, 
    * different emails per page, 
    * when google freezes account
    * Don't Mark progress unless new emails were checked


### Usage
notes:
*GMBME currently handles pages of 100 at a time best, before running set your pagination to display 100 at a time in your gmail settings.*

*I have had google freeze my email account, only for a couple minutes, I assume from too many api calls, it doesnt seem to have any lasting results, but who knows.*

1. Clone the repository
1. navigate to the root folder
1. run `Node index :<emailUsername> *<emailPassword>`
1. wait a while, results are in the `/results/<emailUsername>.json` file.
1. You can also run `node filter :<emailUsername>` to filter the results, the filtered results are placed in `/results/filtered-<emailUsername>.json`.

*Using this software in any way to harm others violates the intended purpose of the software, don't do it please.*

*This software is experimental, I don't know if it can break anything and I would not like to be responsible for the results of you running the program. try it at your own risk.*


Made with fear of google by Quddús George.
