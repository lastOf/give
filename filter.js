let fs = require('fs')
let args = process.argv;
let email = args.filter(el=>el.includes(':'))[0].replace(':', '')
let data = JSON.parse(fs.readFileSync(`results/${email}.json`, 'utf8'));


let removedNot = data.contactArray.filter(el=>!el.email.includes('not'))
console.log(removedNot)
let removedReply = removedNot.filter(el=>!el.email.includes('reply'))
let removedAuto = removedReply.filter(el=>!el.email.includes('auto'))
let removedLong = removedAuto.filter(el=>el.email.length<45)
let removedSupport = removedLong.filter(el=>!el.email.includes('support'))
let removedHelp = removedSupport.filter(el=>!el.email.includes('help'))
let removedInfo = removedHelp.filter(el=>!el.email.includes('info'))
let commonProviders = removedInfo.filter(el=>el.email.includes('gmail')||el.email.includes('hotmail')||email.includes('rr')||email.includes('bellsouth')||email.includes('att')||email.includes('verizon')||email.includes('earth')||email.includes('msn')||email.includes('yahoo')||email.includes('aol')||email.includes('.edu'))

let result = {
    lightFilter:removedInfo,
    heavyFilter:commonProviders
}

console.log(data.contactArray.length-removedInfo.length, "support or mailbot emails removed.")
console.log(removedInfo.length, "Lightly filtered emails.")
console.log(commonProviders.length, "Filtered emails from common providers such as gmail or .edu.",)
let json = JSON.stringify(result)
fs.writeFileSync(`results/filtered-${email}.json`, json, 'utf8')